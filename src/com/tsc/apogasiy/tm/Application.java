package com.tsc.apogasiy.tm;

import com.tsc.apogasiy.tm.constant.ArgumentConst;
import com.tsc.apogasiy.tm.constant.TerminalConst;
import com.tsc.apogasiy.tm.model.Command;
import com.tsc.apogasiy.tm.repository.CommandRepository;
import com.tsc.apogasiy.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        parseArgs(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    private static void displayHelp() {
        for (final Command command: CommandRepository.getCommands()) {
            System.out.println(command);
        }
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Alexey Pogasiy");
        System.out.println("apogasiy@tsconsulting.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayError() {
        System.out.println("Unknown command! Type 'help' for supportable command!");
    }

    private static void displayInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long userMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(userMemory));
    }

    private static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                displayAbout();
                break;
            case TerminalConst.VERSION:
                displayVersion();
                break;
            case TerminalConst.HELP:
                displayHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.INFO:
                displayInfo();
                break;
            default:
                displayError();
                break;
        }
    }

    private static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.INFO:
                displayInfo();
                break;
            default:
                displayError();
                break;
        }
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        for (String param : args)
            parseArg(param);
    }

}
